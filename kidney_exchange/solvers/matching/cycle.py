from kidney_exchange.solvers.matching.round import Round


class Cycle(Round):
    """
    Set of consequtive transplants that starts with donor of some pair X and ends with recipient of pair X
    """
    pass
